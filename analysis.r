# This file is part of debian-contrib-years
#
# Copyright (C) 2023, 2024 Rafael Laboissière
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.

contributors <- read.csv ("contributors.csv")

since <- as.integer (sapply (contributors$Since,
                             function (x) strsplit(x, " ")[[1]] [2]))

count <- table (since)

png (file = "debian-contributors-since-year.png", width = 600)
plot (count,
      main = sprintf ("(generated on %s, for the %d currently active contributors)",
                      as.character (Sys.Date ()), nrow (contributors)),
      xlab = "", las = 2, ylab = "count", lwd = 8, col = "gray")
mtext ("year", side = 1, line = 4)
dummy <- dev.off ()

members <- read.csv("members.csv")
members <- subset (members, Since != "(unknown)")
year <- sapply (members$Since, function (x) strsplit (x, "-") [[1]] [1])
tb.year <- table (year)
member.since <- data.frame (tb.year)
member.since$year <- as.numeric (as.character (member.since$year))
min.year <- min (member.since$year)

library(AER)
fm <- (glm (Freq ~ I(year - min.year), member.since, family = poisson))
dispersiontest (fm)

fm <- (glm (Freq ~ I(year - min.year), member.since, family = quasipoisson))
show (fm)
cf <- coefficients (fm)
cat (sprintf ("Yearly decrease = %.2f%%", 100 * (1 - exp (cf [2]))))
show (summary (fm))

png (file = "debian-new-maintainers-per-year.png", width = 600)
plot (tb.year, lwd = 8, col = "gray", las = 2, ylab = "count", xlab = "")
mtext ("year", side = 1, line = 4)
lines (member.since$year, predict (fm, type = "response"), col = "red", lwd = 3)
dummy <- dev.off ()

