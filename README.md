# Debian community renewal and project obsolescence

## Introduction

This repository contains [Python](get-data.py) and [R](analysis.r) scripts
for automatically processing data presented in the web page at
[contributors.debian.org](https://contributors.debian.org/) and
[nm.debian.org](https://nm.debian.org/members/).

The goal is to find quantitative support for the lack of community renewal
in the realm of the Debian project.

## Results

### Distribution of age of first contribution to Debian

Each vertical line in the figure below represents the amount of active
contributors who started contributing to the Debian project in the
corresponding year. It shows a steep increase in the number of new
contributors between 1995 and the beginning of the millennium and a slow
decrease until today. In the last four years (from 2020 to 2023) the number
of new contributors per year is under 10.

![figure](https://salsa.debian.org/rafael/debian-contrib-years/-/jobs/artifacts/main/raw/debian-contributors-since-year.png?job=figure)

This figure is automatically generated using the CI/CD mechanism of GitLab
(see the [pipeline definition](.gitlab-ci.yml)).

### Number of new members since year 2000

[Mo Zhou](https://people.debian.org/~lumin/)
[suggested](https://lists.debian.org/debian-project/2023/12/msg00013.html)
that the data above seems correlated with the number of new Debian
accounts.

The year of admission of each current Debian developers is gathered form
the table in the [nm.debian.org](https://nm.debian.org/members/) web page.
Some developers in that table have the date indicated as "(unknown)". These
are people who were already in Debian when the Debian New Maintainer
Process was established, in 2000 (see [Wallach, Allan, & Harries,
2005](http://dirichlet.net/pdf/wallach05debian.pdf) for the historical
details).

Considering only the maintainers accepted after 2000, a generalized linear
model was fitted to the data, with the independent factor being the year
and the dependent variable the count per year. The count is modeled from a
quasipoisson distribution (i.e. an [overdispersed Poisson
distribution](https://en.wikipedia.org/wiki/Overdispersion#Poisson)). The
need for a quasipoisson distribution is confirmed by use the
`dispersiontest` function of the R
[AER](https://cran.r-project.org/web/packages/AER/index.html) package
(dispersion = 4.04, significantly different from zero, _z_ = 2.21, _p_ <
0.02).

The data and the predicted trend is shown in the figure below:

![figure](https://salsa.debian.org/rafael/debian-contrib-years/-/jobs/artifacts/main/raw/debian-new-maintainers-per-year.png?job=figure)

The year-dependent coefficient of the model is significantly different from
zero (_t_ = -3.64, _p_ < 0.01). This brings evidence to the fact that the
number of new members in Debian is declining since 2000. Note that the
quasipoisson fitting implies a logarithmic link function. The predicted
curve is hence exponential. From the coefficient of the fitted model we
infer a decrease of 3.68% in the number of new maintainers each year.

## Discussion

Comments by [Sébastien Villemot](https://salsa.debian.org/sebastien), sent
privately to me:

> It seems clear that our community is not renewing itself.
>
> One pessimistic interpretation is that we're an aging group working on an
> increasingly irrelevant project, that what we do doesn't appeal to young
> people, and that Debian will disappear with our generation.
>
> A more optimistic interpretation is that we do our work so well that young
> people find it more useful to do something else. But the day the need
> arises, new people will come along to fill the roles that have become
> vacant.
>
> Given Debian's importance in the free software ecosystem, I'm leaning
> towards the second interpretation, at least at this stage. Although there's
> no doubt that, like any technology, the project will become obsolete sooner
> or later.

A [thread](https://lists.debian.org/debian-project/2023/12/msg00012.html),
in the mailing list debian-project, was triggered by the announcement of
the initial version of the present repository. [Steffen
Möller](https://wiki.debian.org/SteffenMoeller)
[contributed](https://lists.debian.org/debian-project/2023/12/msg00018.html)
by listing the factors that may have influence in the number of active individuals:

- Positive factors
    - Location of DebConf (with many or not so many devs affording to attend)
    - Popular platforms like the Raspberry Pi working with Debian derivative
    - Debian packaging teams on salsa
    - self-education
    - Impression the DD status makes on outsiders/your next employer
    - Pleasant interactions on mailing lists with current or past team members
    - Team building with other DDs on projects of interest

- Negative factors
    - Advent of homebrew+conda
    - Containers
    - Increasing workloads as one ages and does not give packages up
    - Work-life-balance
    - Migrating to upstream
    - Delay between what upstream releases and what is available in our distro
    - Unpleasant interactions on mailing lists with current or past team members

## Author

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

Copyright © 2023, 2024  Rafael Laboissière (<rafael@debian.org>)

The material in this repository can be freely distributed, reproduced and
modified according to the terms of the GNU General Public License as
published by the Free Software Foundation, either [version 3 of the
License](https://www.gnu.org/licenses/gpl-3.0), or (at your option) any
later version.


<!---
Local Variables:
ispell-local-dictionary: "american"
eval: (flyspell-mode)
End:
--->

<!--  LocalWords:  Laboissière GitLab
 -->
