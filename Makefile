# This file is part of debian-contrib-years
#
# Copyright (C) 2023, 2024 Rafael Laboissière
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.

CSV_FILES = contributors.csv members.csv
PNG_FILES = debian-contributors-since-year.png	\
            debian-new-maintainers-per-year.png

$(PNG_FILES): $(CSV_FILES) analysis.r
	Rscript analysis.r

$(CSV_FILES): get-data.py
	./get-data.py

.PHONY: clean
clean:
	rm -f $(CSV_FILES) $(PNG_FILES)
